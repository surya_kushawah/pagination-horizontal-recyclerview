package com.wave.pagination.recyclerview;

import com.wave.pagination.recyclerview.model.NewsItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApiService {

    @GET("newsfeed.php")
    Call<List<NewsItem>> getNewsFeed(@Query("page_no") Integer pageNo);
}
