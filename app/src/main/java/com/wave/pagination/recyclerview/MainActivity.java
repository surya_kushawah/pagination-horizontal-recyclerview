package com.wave.pagination.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.wave.pagination.recyclerview.model.NewsItem;
import com.wave.pagination.recyclerview.utils.EqualSpacingItemDecoration;
import com.wave.pagination.recyclerview.utils.PaginationScrollListener;
import com.wave.pagination.recyclerview.widget.HorizontalSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements HorizontalSwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "MainActivity";

    PaginationAdapter mPaginationAdapter;
    LinearLayoutManager mLayoutManager;

    RecyclerView mRecyclerView;
    HorizontalSwipeRefreshLayout mSwipeRefreshLayout;
    public static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private int totalPage = 3;
    private boolean isLoading = false;
    int itemCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        mRecyclerView = findViewById(R.id.mRecyclerView);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefresh);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        mPaginationAdapter = new PaginationAdapter(new ArrayList<NewsItem>());
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(32, EqualSpacingItemDecoration.HORIZONTAL));
        mRecyclerView.setAdapter(mPaginationAdapter);
        mSwipeRefreshLayout.setRefreshing(true);
        preparedListItem();
        /**
         * add scroll listener while user reach in bottom load more will call
         */
        mRecyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                preparedListItem();

            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    protected void preparedListItem() {

        RestApiService apiService = RetrofitInstance.getApiService();
        Call<List<NewsItem>> call = apiService.getNewsFeed(currentPage);
        Log.d(TAG, "loadItemFromServer: " + currentPage);
        call.enqueue(new Callback<List<NewsItem>>() {
            @Override
            public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {
                if (currentPage != PAGE_START) mPaginationAdapter.removeLoading();
                mPaginationAdapter.addAll(response.body());
                mSwipeRefreshLayout.setRefreshing(false);
                if (currentPage < totalPage) mPaginationAdapter.addLoading();
                else isLastPage = true;
                isLoading = false;
            }

            @Override
            public void onFailure(Call<List<NewsItem>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        itemCount = 0;
        currentPage = PAGE_START;
        isLastPage = false;
        mPaginationAdapter.clear();
        preparedListItem();
    }
}
