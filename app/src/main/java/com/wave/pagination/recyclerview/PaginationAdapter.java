package com.wave.pagination.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.pagination.recyclerview.model.NewsItem;

import java.util.List;


public class PaginationAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;

    private List<NewsItem> mNewsItemList;
    private Callback mCallback;

    public PaginationAdapter(List<NewsItem> news) {
        this.mNewsItemList = news;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
            case VIEW_TYPE_LOADING:
                return new EmptyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }


    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == mNewsItemList.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return mNewsItemList == null ? 0 : mNewsItemList.size();
    }

    public void add(NewsItem response) {
        mNewsItemList.add(response);
        notifyItemInserted(mNewsItemList.size() - 1);
    }

    public void addAll(List<NewsItem> postItems) {
        for (NewsItem response : postItems) {
            add(response);
        }
    }


    private void remove(NewsItem postItems) {
        int position = mNewsItemList.indexOf(postItems);
        if (position > -1) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addLoading() {
        isLoaderVisible = true;
        add(new NewsItem());
    }

    public void removeLoading() {
        isLoaderVisible = false;
        int position = mNewsItemList.size() - 1;
        NewsItem item = getItem(position);
        if (item != null) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    NewsItem getItem(int position) {
        return mNewsItemList.get(position);
    }


    public interface Callback {
        void onRepoEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        TextView newTitle;
        ImageView newsImage;

        public ViewHolder(View itemView) {
            super(itemView);
            newTitle = itemView.findViewById(R.id.txtNewsTitle);
            newsImage = itemView.findViewById(R.id.ivNewsImage);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            NewsItem mNewsItem = mNewsItemList.get(position);
            newTitle.setText(mNewsItem.getMainTitle());
//            Glide.with(itemView.getContext())
//                    .load(mNewsItem.getMediaUrl()).apply(new RequestOptions().override(ScreenUtils.getScreenWidth(itemView.getContext()) - ViewUtils.dpToPx(36), 150).placeholder(R.drawable.picture).error(R.drawable.picture))
//                    .into(newsImage);
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);

        }

        @Override
        protected void clear() {

        }

    }
}
